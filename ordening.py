def find_matching_pair(numbers, target_sum):
    left, right = 0, len(numbers) - 1

    while left < right:
        current_sum = numbers[left] + numbers[right]

        if current_sum == target_sum:
            return numbers[left], numbers[right]
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1

    return None



numbers = [2,3,6,7]
target_sum = 9

matching_pair = find_matching_pair(numbers, target_sum)
if matching_pair:
    info  =["Matching pair:" , str(matching_pair[0]), "+" , str(matching_pair[1]), " = " ,str(target_sum)]       
    print(" ".join(info))
else:
    print("No matching pair found.")
